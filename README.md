# Deprecated: API is now included in frontend and backend project

# Gachou API

This project contains the API definitions of the Gachou
project. 

## External APIs 

Gachou exposes a RESTful API to the public. 
The API is currently described by an [OpenAPI 2.0](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/2.0.md) 
document. 

* [rest-api.yml](./rest-api.yml)
* [HTML-Version on the bootprint-playground](https://bootprint.knappi.org/public-apis/https/gitlab.com/gachou/gachou-api/raw/master/rest-api.yml.html)

## Internal APIs 

There are a number of APIs in Gachou

* [core-api](./src/core.ts): This API is used by the
 express-layer to access core-functions of Gachou. It exposes
 all actions like getting date, modifying the index etc
* [tasks-api](./src/tasks.ts): This API is used by 
 the Gachou-core implementation to run long running and resource intensive
 tasks. It limits the number of tasks being run in parallel and 
 provides a rudimentary api for monitoring them.
* [bidder-api](./src/bidder.ts): Bidder and auctions are used in
 several places in order to determine the best plugin to 
 do a job. For example, there may be multiple normalizer
 plugins, but only one is suitable for videos. The auction
 gives the file to every plugin and asks for a "bid" (a number
 of how well this plugin can handle the file) and then
 gives the file to the plugin with the highest bid. 
* [model](./src/model.ts): This file contains the shared model that is 
 used in the communication between core-api and plugins. 
 This model is also used in some API responses.  

Implementations for these interfaces are part of the 
[gachou-backend](https://gitlab.com/gachou/gachou-backend) project.

## Plugins / Components

The actual work is done by several components with their own API.
Each have a default implementation but can be exchanged (not yet, but 
the goal is  to make this possible).

* [media-store](../../src/api/plugins/media-store.ts): This component handles 
  the storage and retrieval of media data (i.e. thumbnails and original
  media files). It provides functions to upload and download both kinds and
  to iterate all files.
* [metadata-handler](../../src/api/plugins/metadata-handler.ts): This component handles
  the extraction and update of metadata within the media files.
* [metadata-index](../../src/metadata-index/metadata-index.ts): This component 
  manages an index of the metadata as well and provides methods to
  update and search it.
* [normalizer](../../src/api/plugins/normalizer.ts): This component converts
  a media-file to a normalized format that can store XMP-Tags 
* [thumbnailer](../../src/api/plugins/thumbnailer.ts): This component converts
  a media-file to a thumbnail.
  


## Thumbnail specifications

Thumbnail sizes are specified by a string that is parsed by the gachou-core implementation into
an object matching the [IParsedScaleSpec](./src/plugins/thumbnailer.ts)-interface
The format of this string is
 
```
{width}x{height}[-crop]
```

where 

* `{width}` is the target width
* `{height}` is the target height
* `[-crop]` is the optional string `-crop`. If the string is present, the `crop`-property will
  be set to `true` meaning that the image should be cropped to an exact match of the specified size.
  Default is to scale the image to fit within a box of the given size. 

